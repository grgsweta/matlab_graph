%% clear screen comand window
clc
clc
%% start timer
tic,
%% import dataset and insert it into dataset 
dataset= small;
largestdataset= max(dataset(:)); % get largest node from dataset
dimention=largestdataset(1,1)  % select highest node from the array
%% create zero adjacency matrix with the data set 
matrix=zeros(largestdataset,largestdataset);
l=length(matrix);
a1=dataset(:,1);
a2=dataset(:,2);
for i=1:length(dataset)
    a11=a1(i);
    a22=a2(i);
     matrix(a11,a22)=1;
end
matrix;
%% calculate cluster coefficecient of each node
fprintf ('Cluster Coeffiecient for each node')
 nc= cc(matrix, 'directed')
 %% Global Cluster Coefficecient
 fprintf ('Network Cluster Coeffiecient')
clustercoeffiecient=sum(nc)/dimention
%to print the adj matrix dimention 
    fprintf('Adjacency Matrix dimension %d X %d \n',dimention,dimention);
%% to make row/col zero if row and col are sam (Node Loop)
for i=1:l
    for j=1:l
        if i==j
        matrix(i,j)=0;
        end
    end
end
% fprintf('directed unmodified');
matrix;
%% to make it undirected

%     for i=1:l
%         for j=1:l
%             if (matrix(i,j)==matrix (j,i))
%                 matrix(j,i)=0;
%                 
%             end
%         end
%     end
%     
%  fprintf('undirected modified');
%    matrix;
%% to count and print how many degree each node have

% Rdegree= sum(matrix)+sum(matrix')
a=[];
for i=1:length(matrix)
    a1=sum(matrix(i,:));
    a2=sum(matrix(:,i));
    a3=a1+a2;
    a=[a a3];
end
%% To display node number with it's Degree
for i=1:length(matrix)
   fprintf('Node %i has %d  Degree \n',i,a(i))%d degree is the number in degree with same order as x where x is a normal counter for node number
   end 
%% to print the node the highest degree
    fprintf('This Network has %g node\n',dimention)
    fprintf('The Node with highest degree is:\n')
[high, pos] = max(a);    
largestnode= pos
degnode= high

%% For Visualizing the Network Graph      
row=[];
col=[];
w=[];
for i=1:length(matrix)
    for j=1:length(matrix)
        w=[w matrix(i,j)];
         row=[row i];
    end
     col=[col 1:length(matrix)];
end
DG = sparse(row,col,w);
 h = view(biograph(DG,[],'Showarrows','on','ShowWeights','off'))
DG = sparse(row,col,w);
%% For Shortest Path
% [dist,path,pred] = graphshortestpath(DG,1,3)
% set(h.Nodes(path),'Color',[1 0.4 0.4])
%  edges = getedgesbynodeid(h,get(h.Nodes(path),'ID'));
%  set(edges,'LineColor',[1 0 0])
%  set(edges,'LineWidth',1.5)
%% to stop timer
 toc